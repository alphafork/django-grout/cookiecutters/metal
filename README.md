# Metal - Meta Layout for Python Package Templates

Metal or Meta Layout is a template for generating a
[cookiecutter](https://github.com/cookiecutter/cookiecutter) template for
bootstrapping a basic Python package.


## License

[GPL-3.0-or-later](LICENSE)


## Contact

[Alpha Fork Technologies](https://alphafork.com)

Email: [connect@alphafork.com](mailto:connect@alphafork.com)
